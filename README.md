# This is the website of my high school.

In the year 2000 the City Of Athens announced a website building contest between schools of Athens. Of course I decided to take part. I convinced a friend and we began.

I had just been gifted Microsoft FrontPage and I was playing with the controls trying to create websites myself. Little did I know that would end up being my professional career, and that I would grow up to hate visual website building tools. The images were edited with Paint Shop Pro, probably version 4 or 5.

I was impressed by the game *MYST* at the time and I wanted to create a virtual web tour of my school. It was a technical challenge. 

Firstly, we didn't have a digital camera. All the pictures were taken on film, which was then developed and printed at a shop and then the prints were scanned one by one, by me, on an HP scanner which took a considerable amount of time to scan each image.

Then the images were taking too much space and the site would load forever on our measly 56Kbps modems. Thus I had to ramp up the compression and make them small enough to load at some point. Still the whole website, zipped, took 3Mb which required 3 floppies to distribute.

I didn't have a backup system set up either. After I submitted it to the jury my HDD went bust and I lost it. Fortunately they gave it back to me and here it is.

The website did win first prize in 3 of the 4 categories of the contest, if I remember correctly. The prize money was 800.000 drachmas which, by the time we received it (after a legal battle with the school, which wanted to keep it) were somewhere close to 2.5K euros. In today's money this would be over 3K euros if adjusted for inflation.

We went and bought 10 or 20 soccer balls and donated them to the school, and got a nice, expensive computer each: it was the computer that saw me through uni: Pentium 4 clocked at 2.66Ghz with a Radeon 9700 Pro. It was an absolute beast, that card. I still have it.

This is the story of this website, now enjoy it: If you can find a 15" CRT monitor and I.E. 4 or Netscape 6, set it to 800x600 and click [here](https://qwazix.gitlab.io/3rd-highschool-website/index.htm) to see it as was intended. If you don't, well, browse it anyway.